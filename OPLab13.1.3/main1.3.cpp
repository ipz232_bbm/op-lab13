#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main() {
	srand(time(0));
	float dd;
	clock_t start, finish;

	const int SIZE = 200000;
	int arr[SIZE], c;

	for (int i = 0; i < SIZE; i++) {
		arr[i] = -100 + rand() % 201;
		printf("%4d|", arr[i]);
	}

	start = clock();

	for (int i = 1; i < SIZE; i++) {
		c = arr[i];
		for (int j = i - 1; j >= 0 && arr[j] > c; j--) {
			arr[j + 1] = arr[j];
			arr[j] = c;
		}
	}

	finish = clock();
	dd = (float)(finish - start) / CLOCKS_PER_SEC;

	printf("\n");
	for (int i = 0; i < SIZE; i++) {
		printf("%4d|", arr[i]);
	}

	printf("\n\nTIME = %f", dd);

	return 0;
}