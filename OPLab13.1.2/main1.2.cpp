#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main() {
	srand(time(0));
	float dd;
	clock_t start, finish;

	const int SIZE = 200000;
	int arr[SIZE], imin, a;

	for (int i = 0; i < SIZE; i++) {
		arr[i] = -100 + rand() % 201;
		printf("%4d|", arr[i]);
	}

	start = clock();

	for (int i = 0; i < SIZE; i++) {
		imin = i;
		for (int j = i + 1; j < SIZE; j++)
			if (arr[j] < arr[imin]) imin = j;
		a = arr[i];
		arr[i] = arr[imin];
		arr[imin] = a;
	}

	finish = clock();
	dd = (float)(finish - start) / CLOCKS_PER_SEC;

	printf("\n");
	for (int i = 0; i < SIZE; i++) {
		printf("%4d|", arr[i]);
	}

	printf("\n\nTIME = %f", dd);

	return 0;
}