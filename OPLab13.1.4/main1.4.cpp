#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main() {
	srand(time(0));
	float dd;
	clock_t start, finish;

	const int SIZE = 200000;
	int arr[SIZE];

	for (int i = 0; i < SIZE; i++) {
		arr[i] = -100 + rand() % 201;
		printf("%4d|", arr[i]);
	}

	start = clock();

	int step = SIZE / 2;
	while (step > 0)
	{
		for (int i = 0; i < (SIZE - step); i++)
		{
			int j = i;
			while (j >= 0 && arr[j] > arr[j + step])
			{
				int c = arr[j];
				arr[j] = arr[j + step];
				arr[j + step] = c;
				j--;
			}
		}
		step = step / 2;
	}

	finish = clock();
	dd = (float)(finish - start) / CLOCKS_PER_SEC;

	printf("\n");
	for (int i = 0; i < SIZE; i++) {
		printf("%4d|", arr[i]);
	}

	printf("\n\nTIME = %f", dd);

	return 0;
}