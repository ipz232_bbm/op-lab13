#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));
    int arr[16];
    int k = 16, a, b;
    
    printf("������ ������� �������� ������� ������ (����� ����� ����� �� �����, ��������� ����� �����) ");
    do {
        scanf("%d %d", &a, &b);
    } while (b <= a);

    double dd;
    clock_t start, finish;
    start = clock();

    printf("����� �� ����������\n");
    for (int i = 0; i < k; i++)
    {
        arr[i] = a + rand() % (b - a + 1);
        printf("%3d | ", arr[i]);
    }

    for (int i = k / 2 - 1; i >= 0; i--)
    {
        while (1)
        {
            int large = i;
            int left = 2 * i + 1;
            int right = 2 * i + 2;
            if (left < k && arr[left] > arr[large])
                large = left;
            if (right < k && arr[right] > arr[large])
                large = right;
            if (large != i)
            {
                int temp = arr[i];
                arr[i] = arr[large];
                arr[large] = temp;
                i = large;
            }
            else
                break;
        }
    }

    for (int i = k - 1; i > 0; i--)
    {
        int temp = arr[0];
        int j = 0;
        arr[0] = arr[i];
        arr[i] = temp;
        while (1)
        {
            int large = j;
            int left = 2 * j + 1;
            int right = 2 * j + 2;
            if (left < i && arr[left] > arr[large])
                large = left;
            if (right < i && arr[right] > arr[large])
                large = right;
            if (large != j)
            {
                int temp = arr[j];
                arr[j] = arr[large];
                arr[large] = temp;
                j = large;
            }
            else
                break;
        }
    }

    finish = clock();
    dd = ((float)(finish - start) / CLOCKS_PER_SEC);

    printf("\n����� ���� ����������: \n");
    int n1 = k;
    for (int i = 0; i < n1; i++)
        printf("%3d | ", arr[i]);

    printf("\n��� ���������� = %lf", dd);

    return 0;
}
