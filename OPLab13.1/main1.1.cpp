#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>


int main() {
	srand(time(0));
	float dd;
	clock_t start, finish;

	const int SIZE = 200000;
	int arr[SIZE], c, fl;

	for (int i = 0; i < SIZE; i++) {
		arr[i] = -100 + rand() % 201;
		printf("%4d|", arr[i]);
	}

	start = clock();

	do {
		fl = 0;
		for (int i = 1; i < SIZE; i++)
			if (arr[i - 1] > arr[i]) {
				c = arr[i];
				arr[i] = arr[i - 1];
				arr[i - 1] = c;
				fl = 1;
			}
	} while (fl);

	finish = clock();
	dd = (float)(finish - start) / CLOCKS_PER_SEC;

	printf("\n");
	for (int i = 0; i < SIZE; i++) {
		printf("%4d|", arr[i]);
	}

	printf("\n\nTIME = %f", dd);

	return 0;
}